import java.io.*;

class Demo{
	public static void main(String[]args)throws IOException{
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Enter Size Of Array");
		int size = Integer.parseInt(br.readLine());

		int arr[]=new int[size];

		System.out.println("Enter Elements Of Array");
		for(int i=0;i<arr.length;i++){
			arr[i]=Integer.parseInt(br.readLine());	
		}

		int k = 0;
		for(int i = 0;i<arr.length-1;i++){
			if(arr[i]>arr[i+1]){
				k = arr[i];
				break;
			}else{
				k = arr[arr.length-1];
				break;
			}
		}
		System.out.println("Bitonic Point is : "+k);
	}
}
