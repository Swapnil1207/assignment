import java.io.*;

class Demo{
	public static void main(String[]args)throws IOException{
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Enter Size Of Array");
		int size = Integer.parseInt(br.readLine());

		int arr[]=new int[size];

		System.out.println("Enter Elements Of Array");
		for(int i=0;i<arr.length;i++){
			arr[i]=Integer.parseInt(br.readLine());	
		}

		System.out.println("Enter K : ");
		int k = Integer.parseInt(br.readLine());

		int count = 0;
		System.out.println("Number of Pairs are : ");
		for(int i = 0;i<arr.length;i++){
			for(int j = 0;j<arr.length;j++){
				if(arr[i]+arr[j]==k && i!=j){
					count++;
				}
			}
		}
		System.out.println(count);
	}
}
