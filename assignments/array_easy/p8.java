import java.io.*;

class Demo{
	public static void main(String[]args)throws IOException{
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Enter Size Of Array");
		int size = Integer.parseInt(br.readLine());

		int arr[]=new int[size];

		System.out.println("Enter Elements Of Array");
		for(int i=0;i<arr.length;i++){
			arr[i]=Integer.parseInt(br.readLine());	
		}

		System.out.println("Enter D : ");
		int D = Integer.parseInt(br.readLine());

		for(int i = 1;i<=D;i++){
			int k = arr[0];
			for(int j = 0;j<arr.length-1;j++){
				arr[j]=arr[j+1];
			}
			arr[arr.length-1]=k;
		}

		System.out.println("Rotated Array is : ");

		for(int i = 0;i<arr.length;i++){
			System.out.print(arr[i]+" ");
		}
		System.out.println();
	}
}
