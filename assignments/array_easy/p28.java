import java.io.*;

class Demo{
	public static void main(String[]args)throws IOException{
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Enter Size Of Array");
		int size = Integer.parseInt(br.readLine());

		int arr[]=new int[size];

		System.out.println("Enter Elements Of Array");
		for(int i=0;i<arr.length;i++){
			arr[i]=Integer.parseInt(br.readLine());	
		}

		System.out.println("Output : ");
		for(int i = 0;i<arr.length;i++){
			int product = 1;
			for(int j = 0;j<arr.length;j++){
				if(i!=j){
					product *= arr[j];
				}
			}
			System.out.print(product+" ");
		}
		System.out.println();
	}
}
