import java.io.*;

class Demo{
	public static void main(String[]args)throws IOException{
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Enter Size Of Array");
		int size = Integer.parseInt(br.readLine());

		int arr[]=new int[size];

		System.out.println("Enter Elements Of Array");
		for(int i=0;i<arr.length;i++){
			arr[i]=Integer.parseInt(br.readLine());	
		}

		System.out.println("Enter x : ");
		int x = Integer.parseInt(br.readLine());

		System.out.println("Enter y : ");
		int y = Integer.parseInt(br.readLine());

		int a = -1;
		int b = -1;

		for(int i = 0;i<arr.length;i++){
			if(x == arr[i]){
				a = i;
				break;
			}
		}

		for(int i = 0;i<arr.length;i++){
			if(y == arr[i]){
				b = i;
				break;
			}
		}

		if(a==-1 || b==-1){
			System.out.println("Element is Not Present in Array ");
		}else{
			System.out.println("The Difference Between x & y is : " + (b-a));
	
		}
	}
}
