import java.io.*;

class Demo{
	public static void main(String[]args)throws IOException{
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Enter Size Of Array");
		int size = Integer.parseInt(br.readLine());

		int arr[]=new int[size];

		System.out.println("Enter Elements Of Array");
		for(int i=0;i<arr.length;i++){
			arr[i]=Integer.parseInt(br.readLine());	
		}

		System.out.println("Non Repaeting Elements : ");
		for(int i = 0;i<arr.length;i++){
			int count=0;
			for(int j = 0;j<arr.length;j++){
				if(arr[i]==arr[j] && i!=j){
					count++;
				}
			}
			if(count == 0 ){
				System.out.print(arr[i]+ " ");
			}
		}
		System.out.println();
	}
}
