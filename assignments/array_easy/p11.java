import java.io.*;

class Demo{
	public static void main(String[]args)throws IOException{
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Enter Size Of Array");
		int size = Integer.parseInt(br.readLine());

		int arr[]=new int[size];

		System.out.println("Enter Elements Of Array");
		for(int i=0;i<arr.length;i++){
			arr[i]=Integer.parseInt(br.readLine());	
		}
		

		for(int i = 1;i<=arr.length;i++){
			int count = 0;
			for(int j = 0;j<arr.length;j++){
				if(i==arr[j]){
					count++;
				}
			}
			System.out.print(count+" ");
		}
		System.out.println();
	}
}
