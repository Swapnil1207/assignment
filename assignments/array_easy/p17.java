import java.io.*;

class Demo{
	public static void main(String[]args)throws IOException{
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Enter Size Of Array A");
		int size = Integer.parseInt(br.readLine());

		int A[]=new int[size];

		System.out.println("Enter Elements Of Array A");
		for(int i=0;i<A.length;i++){
			A[i]=Integer.parseInt(br.readLine());	
		}

		System.out.println("Enter Size Of Array B");
		int B[]=new int[(Integer.parseInt(br.readLine()))];

		System.out.println("Enter Elements Of Array B");

		for(int i = 0;i<B.length;i++){
			B[i]=Integer.parseInt(br.readLine());
		}

		System.out.println("Enter X");
		int X  = Integer.parseInt(br.readLine());

		System.out.println("Pairs : ");
		for(int i = 0;i<A.length;i++){
			for(int j = 0;j<B.length;j++){
				if(A[i]+B[j]==X){
					System.out.println(A[i]+" "+B[j]);
				}
			}
		}
	}
}
