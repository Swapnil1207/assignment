import java.io.*;

class Demo{
	public static void main(String[]args)throws IOException{
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Enter Size Of Array");
		int size = Integer.parseInt(br.readLine());

		int arr[]=new int[size];

		System.out.println("Enter Elements Of Array");
		for(int i=0;i<arr.length;i++){
			arr[i]=Integer.parseInt(br.readLine());	
		}

		int k = arr[0];
		int count = 0;

		for(int i = 0;i<arr.length;i++){
			if(arr[i] >= k){
				k = arr[i];
			}else{
				System.out.println(0);
				count++;
				break;
			}
		}
		if(count == 0){
			System.out.println(1);
		}
	}
}
