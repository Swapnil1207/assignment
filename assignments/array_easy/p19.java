import java.io.*;

class Demo{
	public static void main(String[]args)throws IOException{
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Enter Size Of Array");
		int size = Integer.parseInt(br.readLine());

		int arr[]=new int[size];

		System.out.println("Enter Elements Of Array");
		for(int i=0;i<arr.length;i++){
			arr[i]=Integer.parseInt(br.readLine());	
		}

		for(int i = 1;i<arr.length-1;i++){
			if(arr[i-1]<arr[i] && arr[i+1]>arr[i]){
				System.out.println("The Element is : "+arr[i]);
				return;
			}
		}
		System.out.println("No Such Element : "+ (-1));
	}
}
