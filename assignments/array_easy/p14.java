import java.io.*;
import java.util.*;

class Demo{
	public static void main(String[]args)throws IOException{
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Enter Size Of Array a");
		int size = Integer.parseInt(br.readLine());

		int a[]=new int[size];

		System.out.println("Enter Elements Of Array a");
		for(int i=0;i<a.length;i++){
			a[i]=Integer.parseInt(br.readLine());	
		}
		
		System.out.println("Enter Size Of Array b");
		int size1 = Integer.parseInt(br.readLine());
		int b[]=new int[size1];

		System.out.println("Enter Elements Of Array b");
		for(int i=0;i<b.length;i++){
			b[i]=Integer.parseInt(br.readLine());	
		}

		
		ArrayList al = new ArrayList();

		for(int i = 0;i<a.length;i++){
			for(int j = 0;j<b.length;j++){
				if(a[i] == b[j]){
					al.add(a[i]);
				}		
			}
		}

		TreeSet ts = new TreeSet(al);
		System.out.println("Number Of Common Elements : "+ts.size());
	}
}
