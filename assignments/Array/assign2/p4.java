//return index of given element
import java.io.*;
class Demo4{
	public static void main(String[]args)throws IOException{
		BufferedReader br=new BufferedReader(new InputStreamReader(System.in));
		System.out.println("ENTER SIZE OF ARRAY: ");
		int arr[]=new int[Integer.parseInt(br.readLine())];
		System.out.println("ENTER ELEMENTS OF ARRAY: ");
		for(int i=0;i<arr.length;i++){
			arr[i]=Integer.parseInt(br.readLine());
		}
		System.out.println("ENTER ELEMENT WHICH HAS TO FIND: ");
		int x=Integer.parseInt(br.readLine());
		for(int i=0;i<arr.length;i++){
			if(arr[i]==x){
				System.out.println("ELEMENT FOUND AT INDEX: "+i);
			}
		}
	}
}
