import java.io.*;
class Demo3{
	public static void main(String[]args)throws IOException{
		BufferedReader br=new BufferedReader(new InputStreamReader(System.in));

		System.out.println("ENTER SIZE OF ARRAY:");
		int arr[]=new int[Integer.parseInt(br.readLine())];
		System.out.println("ENTER ELEMENTS:");
		for(int i=0;i<arr.length;i++){
			arr[i]=Integer.parseInt(br.readLine());
		}


		int count1=0;
		int count2=0;
		for(int i=0;i<arr.length;i++){
			if(arr[i]%2==0){
				count1=count1+arr[i];
			}
			else{
				count2=count2+arr[i];
			}
		}

		
		System.out.println("SUM OF EVEN NUMBERS IS: "+count1);
		System.out.println("SUM OF ODD NUMBERS IS: "+count2);
	}
}
