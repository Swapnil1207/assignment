//Merger two array
import java.io.*;
class Demo9{
	public static void main(String[]args)throws IOException{
		BufferedReader br=new BufferedReader(new InputStreamReader(System.in));

		System.out.println("ENTER SIZE OF 1 ARRAY: ");
		
		int arr1[]=new int[Integer.parseInt(br.readLine())];

		System.out.println("ENTER ELEMENTS:");
		
		for(int i=0;i<arr1.length;i++){
		
			arr1[i]=Integer.parseInt(br.readLine());
		}
		
		System.out.println("ENTER SIZE OF SECOND ARRAY:");

		int arr2[]=new int[Integer.parseInt(br.readLine())];
		
		System.out.println("ENTER ELEMENTS:");

		for(int i=0;i<arr2.length;i++){
			
			arr2[i]=Integer.parseInt(br.readLine());
		}
		int arr3[]=new int[arr1.length+arr2.length];

		for(int i=0;i<arr1.length;i++){
		
			arr3[i]=arr1[i];
		}
		int j=0;
		for(int i=(arr3.length-arr2.length);i<arr3.length;i++){
			
			arr3[i]=arr2[j++];
		}
		System.out.println("THIRD ARRAY IS: ");
		for(int i=0;i<arr3.length;i++){
			System.out.print(arr3[i]+" ");
		}
		System.out.println();
	}
}
