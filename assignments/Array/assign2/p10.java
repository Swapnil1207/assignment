//Add digts of array and print element if its sum is even.
import java.io.*;
class Demo10{
	public static void main(String[]args)throws IOException{
		BufferedReader br=new BufferedReader(new InputStreamReader(System.in));
		System.out.println("ENTER SIZE OF ARRAY: ");

		int arr[]=new int[Integer.parseInt(br.readLine())];
		
		for(int i=0;i<arr.length;i++){
		
			arr[i]=Integer.parseInt(br.readLine());

		
		}
		System.out.println("SUCH NUMBERS ARE:");
		for(int i=0;i<arr.length;i++){
			
			int sum=0;

			int temp1=arr[i];
			while(temp1>0){
				sum=sum+temp1;
				temp1=temp1/10;
			}
			if(sum%2==0){
				System.out.print(arr[i]+" ");
			}
		}
		System.out.println();
	}
}
