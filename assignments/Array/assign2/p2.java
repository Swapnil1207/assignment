//Find the number of even and odd elements of array
import java.io.*;
class Demo2{
	public static void main(String[]args)throws IOException{
		BufferedReader br=new BufferedReader(new InputStreamReader(System.in));
		
		int count=0;
		System.out.println("ENTER SIZE OF ARRAY:");
		int arr[]=new int[Integer.parseInt(br.readLine())];
		System.out.println("ENTER ELEMENTS OF ARRAY:");
		for(int i=0;i<arr.length;i++){
			arr[i]=Integer.parseInt(br.readLine());
			if(arr[i]%2==0){
				count++;
			}
		}
		System.out.println("EVEN NUMBERS="+count);
		System.out.println("ODD NUMBERS="+(arr.length-count));
	}
}
