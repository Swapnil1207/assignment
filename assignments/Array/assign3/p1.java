///Count digits in elements in array
import java.io.*;
class Demo1{
	public static void main(String[]args)throws IOException{
		BufferedReader br=new BufferedReader(new InputStreamReader(System.in));

		
		System.out.println("ENTER SIZE OF ARRAY:");
		int arr[]=new int[Integer.parseInt(br.readLine())];
		System.out.println("ENTER ELEMENTS OF ARRAY:");
		for(int i=0;i<arr.length;i++){
			arr[i]=Integer.parseInt(br.readLine());
		}

		
		for(int i=0;i<arr.length;i++){
			int temp1=arr[i];
			int count=0;
			while(temp1>0){
				temp1=temp1/10;
				count++;
			}
			System.out.println("NUMBER OF DIGITS PRESENT IN: "+arr[i]+" ARE " +count);
		}
	}
}
