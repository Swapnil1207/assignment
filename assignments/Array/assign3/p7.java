//Find Strong Number In Array
import java.io.*;
class Demo7{
	public static void main(String[]args)throws IOException{
		BufferedReader br=new BufferedReader(new InputStreamReader(System.in));

		System.out.println("ENTER SIZE OF ARRAY:");
		int arr[]=new int[Integer.parseInt(br.readLine())];
		System.out.println("ENTER ELEMENTS OF ARRAY:");
		for(int i=0;i<arr.length;i++){
			arr[i]=Integer.parseInt(br.readLine());
		}

		for(int i=0;i<arr.length;i++){
			int temp1=arr[i];
			int sum=0;
			while(temp1>0){
				int rem=temp1%10;
				int mult=1;
				while(rem>0){
					mult=mult*rem;
					rem--;
				}
			sum=sum+mult;
			temp1=temp1/10;	
			}
			if(sum==arr[i]){
				System.out.println("STRONG NUMBER "+arr[i]+" FOUND AT "+i);
			}
		}
	}
}
