//Find Pallindrome Number In Array
import java.io.*;
class Demo5{
	public static void main(String[]args)throws IOException{
		BufferedReader br=new BufferedReader(new InputStreamReader(System.in));

		System.out.println("ENTER SIZE OF ARRAY:");
		int arr[]=new int[Integer.parseInt(br.readLine())];
		System.out.println("ENTER ELEMENTS OF ARRAY:");
		for(int i=0;i<arr.length;i++){
			arr[i]=Integer.parseInt(br.readLine());
		}

		for(int i=0;i<arr.length;i++){
			int temp1=arr[i];
			int sum=0;
			while(temp1>0){
				int rem=temp1%10;
				sum=sum*10+rem;
				temp1=temp1/10;
			}
			if(sum==arr[i]){
				System.out.println("PALLINDROME NUMBER "+arr[i]+" FOUND AT "+i);
			}
		}
	}
}
