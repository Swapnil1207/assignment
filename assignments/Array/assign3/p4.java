//Return Index of Prime Number
import java.io.*;
class Demo4{
	public static void main(String[]args)throws IOException{
		BufferedReader br=new BufferedReader(new InputStreamReader(System.in));

		System.out.println("ENTER SIZE OF ARRAY:");
		int arr[]=new int[Integer.parseInt(br.readLine())];
		System.out.println("ENTER ELEMENTS OF ARRAY:");
		for(int i=0;i<arr.length;i++){
			arr[i]=Integer.parseInt(br.readLine());
		}

		for(int i=0;i<arr.length;i++){
			int count=0;
			for(int j=1;j<=arr[i];j++){
				if(arr[i]%j==0){
					count++;
				}
			}
			if(count==2){
				System.out.println("PRIME NUMBER "+arr[i]+" FOUND AT "+i);
			}
		}
	}
}
