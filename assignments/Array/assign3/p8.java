//Find Armstrong Number In Array
import java.io.*;
class Demo8{
	public static void main(String[]args)throws IOException{
		BufferedReader br=new BufferedReader(new InputStreamReader(System.in));

		System.out.println("ENTER SIZE OF ARRAY:");
		int arr[]=new int[Integer.parseInt(br.readLine())];
		System.out.println("ENTER ELEMENTS OF ARRAY:");
		for(int i=0;i<arr.length;i++){
			arr[i]=Integer.parseInt(br.readLine());
		}

		for(int i=0;i<arr.length;i++){
			int temp1=arr[i];
			int temp2=arr[i];
			int sum=0;
			int count=0;
			while(temp1>0){
				count++;
				temp1=temp1/10;
			}
			while(temp2>0){
				int rem=temp2%10;
				int mult=1;
				for(int j=1;j<=count;j++){
					mult=mult*rem;	
				}
				sum=sum+mult;
				temp2=temp2/10;
			}
			if(sum==arr[i]){
				System.out.println("ARMSTRONG NUMBER "+arr[i]+" FOUND AT "+i);
			}
		}
	}
}
