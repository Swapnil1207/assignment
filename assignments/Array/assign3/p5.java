//Find Perfect Number In Array
import java.io.*;
class Demo5{
	public static void main(String[]args)throws IOException{
		BufferedReader br=new BufferedReader(new InputStreamReader(System.in));

		System.out.println("ENTER SIZE OF ARRAY:");
		int arr[]=new int[Integer.parseInt(br.readLine())];
		System.out.println("ENTER ELEMENTS OF ARRAY:");
		for(int i=0;i<arr.length;i++){
			arr[i]=Integer.parseInt(br.readLine());
		}

		for(int i=0;i<arr.length;i++){
			int temp1=arr[i];
			int sum=0;
			for(int j=1;j<arr[i];j++){
				if(temp1%j==0){
					sum=sum+j;
				}
			}
			if(sum==arr[i]){
				System.out.println("PERFECT NUMBER "+arr[i]+" FOUND AT "+i);
			}
		}
	}
}
