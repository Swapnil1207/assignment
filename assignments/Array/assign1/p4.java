//Print vowels in array
import java.io.*;
class Demo4{
	public static void main(String[]args)throws IOException{
		BufferedReader br=new BufferedReader(new InputStreamReader(System.in));
		System.out.println("ENTER SIZE OF ARRAY: ");
		char arr[]=new char[Integer.parseInt(br.readLine())];
		for(int i=0;i<arr.length;i++){
			arr[i]=(char)br.read();
			br.skip(1);
		}
		for(int i=0;i<arr.length;i++){
			if(arr[i]=='a'||arr[i]=='e'||arr[i]=='i'||arr[i]=='o'||arr[i]=='u'){
				System.out.print(arr[i]+" ");
			}
		}
	}
}
