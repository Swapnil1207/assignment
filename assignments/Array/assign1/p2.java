///print product of even numbers
import java.io.*;
class Demo2{
	public static void main(String[]args)throws IOException{
		BufferedReader br=new BufferedReader(new InputStreamReader(System.in));
		System.out.println("ENTER SIZE OF ARRAY: ");
		int arr[]=new int[Integer.parseInt(br.readLine())];
		for(int i=0;i<arr.length;i++){
			arr[i]=Integer.parseInt(br.readLine());
		}
		int mult=1;
		for(int i=0;i<arr.length;i++){
			if(arr[i]%2==0){
				mult=mult*arr[i];
			}
		}
		System.out.println("MULTIPLICATION IS: "+mult);
	}
}
