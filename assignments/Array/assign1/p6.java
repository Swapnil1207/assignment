import java.io.*;
class Demo6{
	public static void main(String[]args)throws IOException{
		BufferedReader br=new BufferedReader(new InputStreamReader(System.in));
		
		System.out.println("Enter Size of Array :");

		int arr[]=new int[Integer.parseInt(br.readLine())];

		System.out.println("Enter Elements Of Array :");
		
		for(int i=0;i<arr.length;i++){
			arr[i]=Integer.parseInt(br.readLine());
		}

		System.out.println("Elements of Array Are :");

		for(int x :arr){
			System.out.println(x/10);
		}
	}
}
