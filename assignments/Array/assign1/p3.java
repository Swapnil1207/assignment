//print product of odd index Elements
import java.io.*;
class Demo3{
	public static void main(String[]args)throws IOException{
		BufferedReader br=new BufferedReader(new InputStreamReader(System.in));
		System.out.println("ENTER SIZE OF ARRAY: ");
		int arr[]=new int[Integer.parseInt(br.readLine())];
		int mult=1;
		for(int i=0;i<arr.length;i++){
			arr[i]=Integer.parseInt(br.readLine());
			if(i%2==1){
				mult=mult*arr[i];
			}
		}System.out.println("MULTIPLICATION IS: "+mult);
	}
}
