//Print sum of odd numbers
import java.io.*;
class Demo1{
	public static void main(String[]args)throws IOException{
		BufferedReader br=new BufferedReader(new InputStreamReader(System.in));
		System.out.println("ENTER SIZE OF ARRAY:");
		int sum=0;
		int arr[]=new int[Integer.parseInt(br.readLine())];
		for(int i=0;i<arr.length;i++){
			arr[i]=Integer.parseInt(br.readLine());
		
		}
		for(int i=0;i<arr.length;i++){
			if(arr[i]%2==1){
				sum=sum+arr[i];
			}
		}
		System.out.println(sum);
	}
}
