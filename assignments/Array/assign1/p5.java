//print elements divisible by 5
import java.io.*;
class Demo5{
	public static void main(String[]args)throws IOException{
		BufferedReader br=new BufferedReader(new InputStreamReader(System.in));
		System.out.println("ENTER SIZE OF ARRAY: ");
		int arr[]=new int[Integer.parseInt(br.readLine())];
		for(int i=0;i<arr.length;i++){
			arr[i]=Integer.parseInt(br.readLine());
		}
		for(int i=0;i<arr.length;i++){
			if(arr[i]%5==0){
				System.out.println(arr[i]);
			}
		}
	}
}
