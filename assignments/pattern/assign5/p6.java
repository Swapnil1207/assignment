import java.io.*;
class Demo6{
	public static void main(String[]args)throws IOException{
		BufferedReader br=new BufferedReader(new InputStreamReader(System.in));
		System.out.println("ENTER FIRST CHARACTER:");
		char ch1=(char)br.read();
		br.skip(1);
		
		System.out.println("ENTER SECOND CHARACTER:");
		char ch2=(char)br.read();
		br.skip(1);
		
		if(ch1==ch2){
			System.out.println("CHARACTERS ARE EQUAL");
		}else{
			int Diff=ch1-ch2;
			if(Diff<0){
				Diff=Diff*(-1);
			}
			
			System.out.println("THE DIFFERENCE BETWEEN "+ch1+" AND "+ch2+" IS  "+ Diff);		}
	}
}
