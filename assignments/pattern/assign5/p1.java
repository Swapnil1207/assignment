import java.io.*;
class Demo1{
	public static void main(String args[])throws IOException{
		BufferedReader br=new BufferedReader(new InputStreamReader(System.in));
		System.out.println("ENTER NUMBER OF ROWS");
		int rows=Integer.parseInt(br.readLine());
		
		int N=rows;
		
		for(int i=1;i<=rows;i++){
			for(int j=1;j<=rows;j++){
				if(i%2==1){
					System.out.print((char)(64+N)+""+N--+" ");
				}
				else{
					System.out.print((char)(64+N)+""+N+++ " ");
				
				}
			}
			if(i%2==1){
				N++;
			}else{
				N--;
			}
			System.out.println();
		}
	}
}
