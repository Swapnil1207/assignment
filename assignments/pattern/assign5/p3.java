import java.io.*;
class Demo3{
	public static void main(String[]args)throws IOException{
		BufferedReader br=new BufferedReader(new InputStreamReader(System.in));
		System.out.println("ENTER NUMBER OF ROWS:");
		int rows=Integer.parseInt(br.readLine());
		for(int i=1;i<=rows;i++){
			int temp1=(rows-i+1)*i;
			for(int j=1;j<=rows-i+1;j++){
				System.out.print(temp1+" ");
				temp1=temp1-i;
			}System.out.println();
		}
	}
}
