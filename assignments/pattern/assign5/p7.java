import java.io.*;
class Demo7{
	public static void main(String args[])throws IOException{
		BufferedReader br =new BufferedReader(new InputStreamReader(System.in));
		System.out.println("ENTER NUMBER OF ROWS:");
		int rows=Integer.parseInt(br.readLine());
	
		int x=(rows*(rows+1))/2;
		System.out.println(x);
		for(int i=1;i<=rows;i++){
			for(int j=1;j<=i;j++){
				if((i%2==0&&rows%2==0)||(i%2==1&&rows%2==1)){
					System.out.print((char)(64+x)+" ");
				}else{
					System.out.print(x+ " ");
				}
				x--;
			}System.out.println();
		}
	}
}
