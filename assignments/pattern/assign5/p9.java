import java.io.*;
class Demo9{
	public  static void main(String[]args)throws IOException{
		BufferedReader br=new BufferedReader(new InputStreamReader(System.in));
		System.out.println("ENTER NUMBER:");
		int a=Integer.parseInt(br.readLine());
		int sum=0;
		while(a>0){
			int rem=a%10;
			int mult=1;
			while(rem>0){
				mult=mult*rem;
				rem--;
			}a=a/10;
			sum=sum+mult;
		}System.out.println(sum);
	}
}
