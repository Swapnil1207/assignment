import java.io.*;
class Demo4{
	public static void main(String[]args)throws IOException{
		BufferedReader br=new BufferedReader(new InputStreamReader(System.in));
		System.out.println("ENTER STARTING NUMBER:");
		int a=Integer.parseInt(br.readLine());
		System.out.println("ENTER ENDING NUMBER:");
		int b=Integer.parseInt(br.readLine());

		for(int i=b;i>=a;i--){
			if(i%2==0){
				System.out.print(i+" ");
			}
		}System.out.println();
		for(int i=a;i<=b;i++){
			if(i%2==1){
				System.out.print(i+" ");
			}
		}
	}
}
