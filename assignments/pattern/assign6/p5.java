import java.io.*;
class Demo5{
	public static void main(String[]args)throws IOException{
		BufferedReader br=new BufferedReader(new InputStreamReader(System.in));
		
		System.out.println("ENTER LOWER LIMIT : ");
		int N1=Integer.parseInt(br.readLine());
		
		System.out.println("ENTER UPPER LIMIT : ");
		int N2=Integer.parseInt(br.readLine());
	
		for(int i=N1;i<=N2;i++){
			int sum=0;
			for(int j=1;j<i;j++){
				if(i%j==0){
					sum=sum+j;
				}
			}
			if(sum==i){
				System.out.println(i);
			}
		}
	}
}
