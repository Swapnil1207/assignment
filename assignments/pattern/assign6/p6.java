import java.io.*;

class Demo6{
	public static void main(String[]args)throws IOException{
		BufferedReader br=new BufferedReader(new InputStreamReader(System.in));

		for(int i=1;i<=5;i++){
			System.out.println("ENTER NUMBER : ");
			int N1=Integer.parseInt(br.readLine());

			int temp=N1;
			int count=0;

			while(temp>0){
				count++;
				temp=temp/10;
			}
			System.out.println("THE DIGIT COUNT IN "+N1+" IS "+count);
		}
	}
}
