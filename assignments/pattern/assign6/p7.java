import java.io.*;
class Demo7{
	public static void main(String[]args)throws IOException{
		BufferedReader br=new BufferedReader(new InputStreamReader(System.in));
		
		System.out.println("ENTER LOWER LIMIT : ");
		int N1=Integer.parseInt(br.readLine());
		
		System.out.println("ENTER UPPER LIMIT : ");
		int N2=Integer.parseInt(br.readLine());
	
		for(int i=N1;i<=N2;i++){
			int temp=i;
			int sum=0;

			while(temp>0){
				int rem=temp%10;
				sum=sum*10+rem;
				temp=temp/10;
			}
				System.out.println(sum);
		}
	}
}
