import java.io.*;
class Demo9{
	public static void main(String[]args)throws IOException{
		BufferedReader br=new BufferedReader(new InputStreamReader(System.in));
		
		System.out.println("ENTER LOWER LIMIT : ");
		int N1=Integer.parseInt(br.readLine());
		
		System.out.println("ENTER UPPER LIMIT : ");
		int N2=Integer.parseInt(br.readLine());
	
		for(int i=N1;i<=N2;i++){
			int temp=i;
			int temp1=i;
			int sum=0;
			int count=0;

			while(temp>0){
				count++;
				temp=temp/10;
			}

			while(temp1>0){
				int rem=temp1%10;
				int mult=1;
					
				while(rem>0){
					mult=mult*rem--;
				}
				sum=sum+mult;
				temp1=temp1/10;
			}
			if(sum==i){
				System.out.println(sum);
			}
		}
	}
}
