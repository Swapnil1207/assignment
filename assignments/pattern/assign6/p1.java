import java.io.*;
class Demo1{
	public static void main(String[]args)throws IOException{
		BufferedReader br=new BufferedReader(new InputStreamReader(System.in));
		
		System.out.println("ENTER LOWER LIMIT : ");
		int N1=Integer.parseInt(br.readLine());
		
		System.out.println("ENTER UPPER LIMIT : ");
		int N2=Integer.parseInt(br.readLine());

		int count=0;

		for(int i=N1;i<=N2;i++){
			if(i%5==0 && i%2==0){
				count++;
				System.out.println(i);
			}	
		}
		System.out.println(count);
	}
}
