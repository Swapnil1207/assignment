import java.io.*;
class Demo2{
	public static void main(String[]args)throws IOException{
		BufferedReader br=new BufferedReader(new InputStreamReader(System.in));

		System.out.println("ENTER NUMBER:");
		int N1=Integer.parseInt(br.readLine());

		switch(N1){
			case 0:
				System.out.println("ZERO");
				break;
			case 1:
				System.out.println("ONE");
				break;
			case 2:
				System.out.println("TWO");
				break;
			case 3:
				System.out.println("THREE");
				break;
			case 4:
				System.out.println("FOUR");
				break;
			case 5:
				System.out.println("FIVE");
				break;
			default:
				System.out.println(N1+">5");
		}
	}
}
