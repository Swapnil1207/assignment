import java.io.*;
class Demo3{
	public static void main(String[]args)throws IOException{
		BufferedReader br=new BufferedReader(new InputStreamReader(System.in));
		
		System.out.println("ENTER 1ST NUMBER:");
		int N1=Integer.parseInt(br.readLine());
		
		System.out.println("ENTER SECOND NUMBER:");
		int N2=Integer.parseInt(br.readLine());

		if(N1<0||N2<0){
			System.out.println("NEGATIVE NUMBER NOT ALLOWED");
		
		}else if(N1>0 && N2>0){
			int mult=N1*N2;
			int rem=mult%2;
			switch(rem){
				case 0:
					System.out.println("EVEN NUMBER");
					break;
				case 1:
					System.out.println("ODD NUMBER");
					break;
			}
		}
	}
}
