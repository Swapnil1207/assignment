import java.io.*;
import java.util.*;

class Demo{
	public static void main(String[]args)throws IOException{
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Enter Size Of First Array : ");
		int size1 = Integer.parseInt(br.readLine());
		int arr1[]=new int[size1];
		System.out.println("Enter Elements Of Array 1");
		for(int i = 0;i<arr1.length;i++){
			arr1[i]=Integer.parseInt(br.readLine());
		}
		
		System.out.println("Enter Size Of Second Array : ");
		int size2 = Integer.parseInt(br.readLine());
		int arr2[]=new int[size2];
		System.out.println("Enter Elements Of Array 2");
		for(int i = 0;i<arr2.length;i++){
			arr2[i]=Integer.parseInt(br.readLine());
		}

		System.out.println("Enter Size Of Third Array : ");
		int size3 = Integer.parseInt(br.readLine());
		int arr3[]=new int[size3];
		System.out.println("Enter Elements Of Array 3");
		for(int i = 0;i<arr3.length;i++){
			arr3[i]=Integer.parseInt(br.readLine());
		}

		System.out.println("Common Elements in all Sorted Array are : ");

		TreeSet ts = new TreeSet();
		for(int i = 0;i<arr1.length;i++){
			for(int j = 0;j<arr2.length;j++){
				if(arr1[i]==arr2[j]){
					ts.add(arr1[i]);
				}
			}
		}
		Iterator itr = ts.iterator();
		while(itr.hasNext()){
			int x = (int)itr.next();
			for(int i = 0;i<arr3.length;i++){
				if(x==arr3[i]){
					System.out.print(x+" ");
				}
			}
		}
		System.out.println();
	}
}
