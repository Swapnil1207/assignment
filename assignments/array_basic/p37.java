import java.io.*;
import java.util.*;

class Demo{
	public static void main(String[]args)throws IOException{
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Enter Size Of Array");
		int size = Integer.parseInt(br.readLine());

		int arr[]=new int[size];

		System.out.println("Enter Elements Of Array");
		for(int i=0;i<arr.length;i++){
			arr[i]=Integer.parseInt(br.readLine());	
		}

		ArrayList al1 = new ArrayList();
		ArrayList al2 = new ArrayList();

		for(int i = 0;i<arr.length;i++){
			if(arr[i]<0){
				al1.add(arr[i]);
			}else{
				al2.add(arr[i]);
			}
		}

		System.out.println("Output is : ");
		int j = 0;
		for(int i = 0;i<arr.length;i++){
			if(i<al1.size()){
				arr[i] = (int)al1.get(i);
			}else{
				arr[i] = (int)al2.get(j++);
			}
			System.out.print(arr[i]+" ");
		}
		System.out.println();
	}
}
