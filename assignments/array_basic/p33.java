import java.io.*;

class Demo{
	public static void main(String[]args)throws IOException{
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Enter Size Of Array");
		int size = Integer.parseInt(br.readLine());

		int arr[]=new int[size];

		System.out.println("Enter Elements Of Array");
		for(int i=0;i<arr.length;i++){
			arr[i]=Integer.parseInt(br.readLine());	
		}
		
		int k = arr.length/2;

		int sum1 = 0;
		int sum2 = 0;
		for(int i = 0;i<arr.length;i++){
			if(i<k){
				sum1 += arr[i];
			}else{
				sum2 += arr[i];
			}
		}

		System.out.println("Multiplication Of Array Sum : "+(sum1*sum2));
	}
}
