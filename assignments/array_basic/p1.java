import java.io.*;

class Demo{
	public static void main(String[]args)throws IOException{
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Enter Size Of Array");
		int size = Integer.parseInt(br.readLine());

		int arr[]=new int[size];

		System.out.println("Enter Elements Of Array");
		for(int i=0;i<arr.length;i++){
			arr[i]=Integer.parseInt(br.readLine());	
		}

		System.out.println("Enter Element : ");
		int x = Integer.parseInt(br.readLine());

		int k = -1;

		for(int i = 0;i<arr.length;i++){
			if(arr[i]==x){
				k = i;
				break;
			}
		}
		System.out.println("Element "+x+" found at Index "+k);
	}
}
