import java.io.*;

class Demo{
	public static void main(String[]args)throws IOException{
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Enter Size Of Array");
		int size = Integer.parseInt(br.readLine());

		int arr[]=new int[size];

		System.out.println("Enter Elements Of Array");
		for(int i=0;i<arr.length;i++){
			arr[i]=Integer.parseInt(br.readLine());	
		}

		for(int i = 0;i<arr.length;i=i+2){
			int x = arr[i];
			arr[i]=arr[i+1];
			arr[i+1]=x;
		}

		System.out.println("*******************************");
		for(int i = 0;i<arr.length;i++){
			System.out.println(arr[i]);
		}
	}
}
