import java.io.*;

class Demo{
	public static void main(String[]args)throws IOException{
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Enter Size Of Array");
		int size = Integer.parseInt(br.readLine());

		int arr[]=new int[size];

		System.out.println("Enter Elements Of Array");
		for(int i=0;i<arr.length;i++){
			arr[i]=Integer.parseInt(br.readLine());	
		}

		System.out.println("Enter K : ");
		int k = Integer.parseInt(br.readLine());
	
		if(k<=arr[0]){
			System.out.println("The Closest Number is : "+arr[0]);
			return;
		}
		if(k>=arr[arr.length-1]){
			System.out.println("The Closest Number is : "+arr[arr.length-1]);
			return;
		}

		int x = 0;
		for(int i = 0;i<arr.length;i++){
			if(arr[i]>k){
				x = i;
				break;
			}
		}
		int a = arr[x]-k;
		int b = k-arr[x-1];

		if(a<b){
			System.out.println("The Closest Number is : "+arr[x]);
		}else if(a>b){
			System.out.println("The Closest Number is : "+arr[x-1]);
		}else{
			System.out.println("The Closest Number is : "+arr[x]);
		}
	}
}
