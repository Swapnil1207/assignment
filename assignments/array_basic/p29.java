import java.io.*;

class Demo{
	public static void main(String[]args)throws IOException{
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Enter String : ");
		String str = br.readLine();

		char arr[]=str.toCharArray();

		for(int i = arr.length-1;i>=0;i--){
			if(arr[i]=='1'){
				System.out.println("Last Index of One : "+i);
				return;
			}
		}
		System.out.println("Last Index of One : "+(-1));
	}
}
