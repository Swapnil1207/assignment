import java.io.*;

class Demo{
	public static void main(String[]args)throws IOException{
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Enter N");
		int n1 = Integer.parseInt(br.readLine());
		
		int n2 = n1;
		int count = 0;

		while(n2>0){
			count++;
			n2 = n2/10;
		}

		int arr[]=new int[count];

		for(int i = 0;i<arr.length;i++){
			arr[i]=5;
			int rem = n1%10;
			
			if(rem!=0){
				arr[i]=rem;
			}
			n1 = n1/10;
		}

		
		/*for(int i = 0;i<arr.length;i++){
			System.out.println(arr[i]);
		}*/
		
		int k = 0;
		for(int i = arr.length-1;i>=0;i--){
			k =k*10+arr[i];
		}

		System.out.println("Output is : "+k);
	}
}
