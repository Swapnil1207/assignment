import java.io.*;
import java.util.*;

class Demo{
	public static void main(String[]args)throws IOException{
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Enter Size Of Array");
		int size = Integer.parseInt(br.readLine());

		int arr[]=new int[size];

		System.out.println("Enter Elements Of Array");
		for(int i=0;i<arr.length;i++){
			arr[i]=Integer.parseInt(br.readLine());	
		}

		int min1 = 0;
		int min2 = 0;

		Arrays.sort(arr);
		min1 = arr[0];
		for(int i = 0;i<arr.length;i++){
			if(arr[i]>min1){
				min2 = arr[i];
				break;
			}
		}
		System.out.println("Smallest Element : "+min1);
		System.out.println("Second Smallest Element : "+min2);
	}
}
