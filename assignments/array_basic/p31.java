import java.io.*;

class Demo{
	public static void main(String[]args)throws IOException{
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Enter Size Of Array");
		int size = Integer.parseInt(br.readLine());

		int arr[]=new int[size];

		System.out.println("Enter Elements Of Array");
		for(int i=0;i<arr.length;i++){
			arr[i]=Integer.parseInt(br.readLine());	
		}
		int k = 0;
		for(int i = 0;i<arr.length;i++){
			for(int j = 0;j<arr.length;j++){
				if(j>i){
					int rem = arr[j]-arr[i];
					k += rem;
				}
			}
		}
		System.out.println("Output : "+k);
	}
}
