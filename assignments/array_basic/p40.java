import java.io.*;

class Demo{
	public static void main(String[]args)throws IOException{
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Enter Size Of Array 1");
		int size = Integer.parseInt(br.readLine());

		int arr[]=new int[size];

		System.out.println("Enter Elements Of Array 1");
		for(int i=0;i<arr.length;i++){
			arr[i]=Integer.parseInt(br.readLine());	
		}
		
		System.out.println("Enter Size Of Array 2");
		int size2 = Integer.parseInt(br.readLine());

		int arr2[]=new int[size2];

		System.out.println("Enter Elements Of Array 1");
		for(int i=0;i<arr2.length;i++){
			arr2[i]=Integer.parseInt(br.readLine());	
		}

		int A = 0;
		int B = 0;

		for(int i = 0;i<arr.length;i++){
			if(arr[i]>arr2[i]){
				A++;
			}else if(arr[i]<arr2[i]){
				B++;
			}
		}
		if(A>B){
			System.out.println("A is Winner");
		}else if(A<B){
			System.out.println("B is Winner");
		}else{
			System.out.println("War Tied");
		}
	}
}
