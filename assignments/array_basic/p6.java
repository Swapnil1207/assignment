import java.io.*;

class Demo{
	public static void main(String[]args)throws IOException{
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Enter Size Of Array");
		int size = Integer.parseInt(br.readLine());

		int arr[]=new int[size];

		System.out.println("Enter Elements Of Array");
		for(int i=0;i<arr.length;i++){
			arr[i]=Integer.parseInt(br.readLine());	
		}

		System.out.println("Enter A : ");
		int a = Integer.parseInt(br.readLine());
		
		System.out.println("Enter B : ");
		int b = Integer.parseInt(br.readLine());
		
		boolean x = false;
		boolean y = false;

		for(int i=0;i<arr.length;i++){
			if(arr[i]==a){
				x = true;
			}
			if(arr[i]==b){
				y = true;
			}
		}

		if(x&&y){
			System.out.println("Output : Yes");	
		}else{
			System.out.println("Output : No");
		}	
	}
}
