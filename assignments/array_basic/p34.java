import java.io.*;
import java.util.*;

class Demo{
	public static void main(String[]args)throws IOException{
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Enter Size Of Array");
		int size = Integer.parseInt(br.readLine());

		int arr[]=new int[size];

		System.out.println("Enter Elements Of Array");
		for(int i=0;i<arr.length;i++){
			arr[i]=Integer.parseInt(br.readLine());	
		}

		Arrays.sort(arr);

		int a = 0;
		int b = arr.length-1;

		ArrayList al = new ArrayList();

		for(int i = 0;i<arr.length;i++){
			al.add(arr[b--]);
			
			if(a == b){
				break;
			}	
			
			al.add(arr[a++]);

			if(a == b){
				break;
			}
		}

		System.out.println("Output is : " +al);
		/*for(int i = 0;i<arr.length;i++){
			arr[i]=(int)al.get(arr[i]);
			System.out.print(arr[i]+" ");
		}*/
	}
}
