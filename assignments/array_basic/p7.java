import java.io.*;
import java.util.*;

class Demo{
	public static void main(String[]args)throws IOException{
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Enter Size Of Array");
		int size = Integer.parseInt(br.readLine());

		int arr[]=new int[size];

		System.out.println("Enter Elements Of Array");
		for(int i=0;i<arr.length;i++){
			arr[i]=Integer.parseInt(br.readLine());	
		}

		Arrays.sort(arr);

		int k = 0;
		for(int i = arr.length-1;i>=0;i--){
			k = k*10+arr[i];
		}

		System.out.println("Largest Number is : "+k);
	}
}
