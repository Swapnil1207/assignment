import java.io.*;

class Demo{
	public static void main(String[]args)throws IOException{
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Enter Size Of Array");
		int size = Integer.parseInt(br.readLine());

		int arr[]=new int[size];

		System.out.println("Enter Elements Of Array");
		for(int i=0;i<arr.length;i++){
			arr[i]=Integer.parseInt(br.readLine());	
		}

		System.out.println("The Output : ");

		int max = arr[arr.length-1];
		System.out.print(max+" ");
		for(int i = arr.length-1;i>=0;i--){
			if(arr[i]>max){	
				max = arr[i];
				System.out.print(max+ " ");
			}
		}
		System.out.println();
	}
}
