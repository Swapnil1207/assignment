import java.io.*;

class Demo{
	public static void main(String[]args)throws IOException{
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Enter Size Of Array A");
		int size = Integer.parseInt(br.readLine());

		int A[]=new int[size];

		System.out.println("Enter Elements Of Array A");
		for(int i=0;i<A.length;i++){
			A[i]=Integer.parseInt(br.readLine());	
		}
		
		System.out.println("Enter Size Of Array B");
		int size1 = Integer.parseInt(br.readLine());

		int B[]=new int[size1];

		System.out.println("Enter Elements Of Array B");
		for(int i=0;i<B.length;i++){
			B[i]=Integer.parseInt(br.readLine());	
		}

		int max = A[0];
		for(int i = 0;i<A.length;i++){
			if(A[i]>max){
				max = A[i];
			}
		}

		int min = A[0];
		for(int i = 0;i<B.length;i++){
			if(B[i]<min){
				min = B[i];
			}
		}

		System.out.println("The Product of Max and Min is : "+ (max*min));
	}
}
