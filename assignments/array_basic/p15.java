import java.io.*;
import java.util.*;

class Demo{
	public static void main(String[]args)throws IOException{
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Enter Size Of Array");
		int size = Integer.parseInt(br.readLine());

		int arr[]=new int[size];

		System.out.println("Enter Elements Of Array");
		for(int i=0;i<arr.length;i++){
			arr[i]=Integer.parseInt(br.readLine());	
		}

		ArrayList al = new ArrayList();

		for(int i = 0;i<arr.length;i++){
			al.add(arr[i]);
		}
		
		TreeSet ts = new TreeSet(al);

		int sum = 0;

		Iterator itr = ts.iterator();

		while(itr.hasNext()){
			sum += (int)itr.next();
		}
		System.out.println("Sum of Distinct Elements is : "+sum);
	}
}
