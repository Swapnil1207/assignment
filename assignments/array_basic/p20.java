import java.io.*;

class Demo{
	public static void main(String[]args)throws IOException{
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Enter Size Of Array");
		int size = Integer.parseInt(br.readLine());

		int arr[]=new int[size];

		System.out.println("Enter Elements Of Array");
		for(int i=0;i<arr.length;i++){
			arr[i]=Integer.parseInt(br.readLine());	
		}

		System.out.println("Enter X : ");
		int x = Integer.parseInt(br.readLine());

		System.out.println("Pair Existance : ");
		for(int i = 0;i<arr.length;i++){
			for(int j = 0;j<arr.length;j++){
				if(arr[i]+arr[j]==x && i!=j){
					System.out.println("Yes");
					return;
				}
			}
		}
		System.out.println("No");
	}
}
