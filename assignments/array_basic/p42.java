import java.io.*;

class Demo{
	public static void main(String[]args)throws IOException{
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Enter Size Of Array");
		int size = Integer.parseInt(br.readLine());

		int arr[]=new int[size];

		System.out.println("Enter Elements Of Array");
		for(int i=0;i<arr.length;i++){
			arr[i]=Integer.parseInt(br.readLine());	
		}
		
		System.out.println("Enter num1 : ");
		int num1 = Integer.parseInt(br.readLine());

		System.out.println("Enter num2 : ");
		int num2 = Integer.parseInt(br.readLine());

		int count = 0;

		int a = 0;
		int b = 0;

		for(int i = 0;i<arr.length;i++){
			if(arr[i]==num1){
				a = i;
			}	
			if(arr[i]==num2){
				b = i;
			}
		}

		System.out.println("The Number of Elements Between num1 & num2 : "+Math.abs(b-a-1));
	}
}
